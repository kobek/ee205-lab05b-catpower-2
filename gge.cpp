/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Kobe Uyeda <kobek@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "gge.h"

double fromGasolineGallonEquivalentToJoule( double gasolineGallon ) {
   return gasolineGallon / GASOLINE_GALLON_EQUIVALENT_IN_A_JOULE;
}

double fromJouleToGasolineGallonEquivalent( double joule ) {
   return joule * GASOLINE_GALLON_EQUIVALENT_IN_A_JOULE;
}
