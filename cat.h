///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Kobe Uyeda <kobek@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

const double CAT_POWER_IN_A_JOULE      = 0;
const char CAT_POWER                   = 'c';

extern double fromCatPowerToJoule ();

extern double fromJouleToCatPower();

